import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  posts = [
    {
      title: 'Ad quis sunt consequat reprehenderit',
      content: 'Laborum dolore velit exercitation eu. Reprehenderit sit eiusmod aliquip ipsum ut. ' +
        'Dolor tempor esse fugiat eiusmod reprehenderit eu non quis dolor aute dolore magna. Aliquip ' +
        'culpa sit anim qui aliquip culpa. Est velit ut aliquip officia adipisicing officia reprehenderit ' +
        'aliqua. Aute occaecat elit mollit occaecat. Ea mollit nulla tempor ea consectetur aliquip.\r\n' +
        'Deserunt ea adipisicing laboris labore deserunt occaecat deserunt sint. Occaecat cupidatat ' +
        'consequat minim fugiat labore sint est deserunt occaecat ad enim culpa sunt ut. Deserunt laborum ' +
        'elit enim et sit pariatur cupidatat velit minim voluptate. Eiusmod minim consequat non ex labore ' +
        'nisi cupidatat excepteur enim id ea quis.\r\n',
      loveIts: -3,
      created_at: '2016-02-09T03:00:17'
    },
    {
      title: 'Anim ipsum commodo veniam voluptate',
      content: 'Aliquip ut amet sint ea id non nisi irure eu et ipsum exercitation in. Elit proident ' +
        'tempor eiusmod in anim Lorem consectetur eu et veniam tempor labore. Irure anim quis nulla ' +
        'ullamco anim ex officia voluptate labore aliqua. Minim laborum aute enim dolor aute reprehenderit ' +
        'velit dolore mollit do enim.\r\nEst do voluptate mollit reprehenderit exercitation aute ex qui ' +
        'culpa adipisicing excepteur ullamco fugiat nulla. Ea do fugiat enim ad nulla aute nisi dolor ' +
        'incididunt ea et. Aute incididunt do laboris occaecat commodo do aliqua aliqua aliquip sunt dolor ' +
        'aute. Sunt fugiat in velit eiusmod non laboris in aliquip occaecat nisi qui aute ut ex. Ipsum mollit ' +
        'dolore ullamco et adipisicing tempor commodo consectetur qui. Nulla fugiat ea cillum proident ' +
        'excepteur ipsum enim. In aliqua reprehenderit sint sit mollit do est in.\r\n',
      loveIts: 5,
      created_at: '2015-06-03T09:16:08'
    },
    {
      title: 'Laborum nostrud quis minim in',
      content: 'Culpa in ut laborum cillum ex aute excepteur qui magna in pariatur ut labore. Deserunt ' +
        'aliquip aliquip non eu culpa velit ut eiusmod fugiat deserunt voluptate sint laborum. Lorem ' +
        'occaecat nostrud aliquip est sint qui voluptate. Id fugiat excepteur dolore aliquip nostrud. ' +
        'Dolore ex sit nulla laboris magna quis anim eiusmod consectetur proident cillum.\r\nIrure in ' +
        'ullamco ea sunt. Quis do enim occaecat dolore exercitation. Lorem velit tempor est veniam mollit ' +
        'est ullamco sunt est mollit duis tempor. Pariatur ullamco incididunt ut esse Lorem anim sunt ' +
        'velit deserunt anim.\r\n',
      loveIts: 10,
      created_at: '2016-01-22T09:54:06'
    },
    {
      title: 'Lorem elit incididunt laboris est',
      content: 'Cillum anim tempor in sint esse cupidatat id. Enim minim labore laborum excepteur sunt ' +
        'mollit sint proident tempor sunt. Deserunt ex fugiat non ullamco et nulla dolor et nisi id ea ' +
        'magna id. Fugiat reprehenderit est occaecat sint cillum. Ut anim proident adipisicing velit ' +
        'voluptate minim aliqua ex aliqua aliquip tempor enim sunt non. Proident magna culpa velit mollit ' +
        'ipsum voluptate ad ea minim ipsum nisi exercitation eiusmod.\r\nAnim occaecat magna consequat sit ' +
        'deserunt duis id mollit officia. Eu velit et fugiat adipisicing qui et culpa veniam dolor laboris. ' +
        'Cupidatat elit tempor consectetur sint qui qui nisi. Aute occaecat sint esse nulla nulla eiusmod ' +
        'sunt eu minim. Incididunt consectetur cupidatat irure ut proident amet reprehenderit sunt qui ea ' +
        'qui aute incididunt.\r\n',
      loveIts: -8,
      created_at: '2016-12-10T08:33:31'
    },
    {
      title: 'Velit nisi adipisicing do esse',
      content: 'Minim tempor laboris adipisicing anim tempor consequat. Sit incididunt laboris veniam amet sit ' +
        'consequat veniam voluptate. Qui tempor magna ad incididunt ipsum voluptate deserunt commodo eiusmod sint ' +
        'ad aute officia non. Deserunt tempor aute culpa dolore.\r\nAd magna veniam ut sunt duis id culpa. Ad ' +
        'cupidatat nostrud sunt commodo elit cillum consequat sunt. Laboris labore laborum magna culpa aliqua. ' +
        'Veniam esse in eiusmod laboris commodo mollit id culpa culpa dolore aute nulla consequat. Deserunt qui ' +
        'sit et non culpa enim aute nostrud.\r\n',
      loveIts: -5,
      created_at: '2015-06-16T10:39:26'
    },
    {
      title: 'Sit sint Lorem irure excepteur',
      content: 'Do labore ullamco sit consectetur mollit laborum esse laboris in. Aute et do laboris mollit ' +
        'cupidatat minim amet excepteur ipsum. Velit occaecat consequat fugiat consectetur ad elit adipisicing ' +
        'reprehenderit ex quis deserunt irure aliquip. Esse velit laboris ullamco est pariatur sunt aute occaecat. ' +
        'Esse excepteur laboris ea ut.\r\nAmet cupidatat Lorem et minim sint sint esse qui id id in pariatur. ' +
        'Anim incididunt deserunt anim commodo. Fugiat fugiat officia magna laboris consectetur. Non do et ' +
        'cupidatat non irure est irure. Nostrud officia reprehenderit esse tempor do id consequat officia ad ' +
        'sunt tempor ex. Nostrud amet quis exercitation dolor laboris elit elit dolore ipsum non.\r\n',
      loveIts: 1,
      created_at: '2016-12-09T08:44:26'
    },
    {
      title: 'In aliquip occaecat aliqua ut',
      content: 'Voluptate commodo mollit incididunt ut deserunt ipsum nostrud exercitation aliquip excepteur. ' +
        'Occaecat enim excepteur do incididunt est. Non pariatur ea consectetur nisi pariatur nisi est aliquip ' +
        'quis. Anim cupidatat excepteur minim pariatur sit et quis sit amet. Consectetur cupidatat dolor id culpa ' +
        'nostrud mollit mollit. Ipsum enim esse labore dolore officia mollit irure.\r\nPariatur irure ut velit ' +
        'in anim amet. Pariatur exercitation qui velit est id commodo id cupidatat. Aute exercitation reprehenderit ' +
        'culpa veniam Lorem adipisicing eu enim nisi dolore aliqua fugiat eiusmod. Labore aute Lorem veniam esse ' +
        'anim eu tempor eiusmod. Incididunt labore aliquip adipisicing aliqua consectetur aute id occaecat.\r\n',
      loveIts: 6,
      created_at: '2016-07-01T07:39:58'
    },
    {
      title: 'Nulla aliquip adipisicing ut ullamco',
      content: 'Culpa eiusmod excepteur dolor exercitation. Nisi excepteur exercitation culpa officia elit ut ' +
        'aliquip. Fugiat culpa esse nulla et irure exercitation enim nisi aliquip amet. Aliquip id incididunt ' +
        'proident ipsum deserunt tempor proident minim eiusmod veniam et amet mollit aute. Occaecat amet eiusmod ' +
        'in eu ullamco. Nulla duis non consequat officia officia nisi.\r\nLaborum eu aute laboris anim eiusmod. ' +
        'Occaecat nostrud consectetur cupidatat cillum incididunt adipisicing sit quis laborum excepteur. Ad ' +
        'excepteur enim eu anim ea esse mollit non reprehenderit consectetur non irure enim. Aute id nulla ad ' +
        'eiusmod laboris et commodo labore sunt minim velit. Magna quis excepteur velit nostrud nulla ad culpa ' +
        'ut consectetur eu nisi. Lorem tempor quis anim laboris enim reprehenderit nisi fugiat voluptate ' +
        'consequat aute enim elit voluptate. Deserunt pariatur proident ad ad nostrud irure aute.\r\n',
      loveIts: -9,
      created_at: '2018-03-27T11:32:55'
    },
    {
      title: 'excepteur in magna aliqua amet',
      content: 'Velit ipsum labore proident dolore aute officia consectetur proident cillum duis fugiat irure. ' +
        'Sunt mollit esse proident culpa. Nostrud labore est irure laboris voluptate qui commodo deserunt sunt ' +
        'eiusmod sunt est.\r\nCommodo cupidatat laboris fugiat laboris. Minim fugiat duis non id ea. Et cillum ' +
        'magna dolor reprehenderit id mollit officia excepteur. Anim veniam minim quis laborum eiusmod deserunt ' +
        'officia enim elit exercitation qui ea veniam. Reprehenderit officia laboris consequat Lorem eu nisi. ' +
        'Ipsum sit aute aliqua ex eu ut id est ex consequat fugiat dolore ut incididunt.\r\n',
      loveIts: -7,
      created_at: '2014-05-28T05:02:07'
    },
    {
      title: 'Pariatur cillum cillum consectetur ex',
      content: 'Irure fugiat reprehenderit non duis voluptate minim id reprehenderit adipisicing veniam ' +
        'fugiat voluptate velit. Eiusmod irure anim tempor pariatur occaecat enim cillum duis minim velit ' +
        'velit duis irure sunt. Non consectetur anim dolor voluptate occaecat do aliqua sunt cillum ex. ' +
        'Enim mollit reprehenderit exercitation duis non cupidatat quis excepteur sunt adipisicing culpa ' +
        'nulla proident proident. Ipsum aliqua voluptate sit sit est dolor excepteur.\r\nDolore do fugiat ' +
        'anim anim sint amet duis. Ullamco laboris deserunt Lorem eu excepteur officia ipsum nisi minim esse. ' +
        'Ad deserunt in adipisicing labore. Sint velit aliquip cupidatat est Lorem ad qui aute. Et anim Lorem ' +
        'nostrud qui eu et nisi proident. Commodo qui ullamco dolore eiusmod Lorem aute do Lorem. Magna incididunt ' +
        'incididunt ex sint est qui aliquip reprehenderit cupidatat quis cillum reprehenderit aliqua excepteur.\r\n',
      loveIts: -8,
      created_at: '2014-11-30T08:59:53'
    }
  ];

}
